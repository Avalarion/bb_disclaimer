<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Bbdisclamer',
	'Disclamer'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_' . bbdisclamer;
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform,bodytext;;9;richtext:rte_transform[flag=rte_enabled|mode=ts_css];3-3-3, rte_enabled, text_properties';
t3lib_extMgm::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_' .bbdisclamer. '.xml');

//$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_'] = 'pi_flexform,bodytext;;9;richtext:rte_transform//[flag=rte_enabled|mode=ts_css];3-3-3, rte_enabled, text_properties';
//t3lib_extMgm::addPiFlexformValue($_EXTKEY.'_pi1', 'FILE:EXT:'.$_EXTKEY.'/flexform_ds.xml');




t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'BB Disclamer');


?>